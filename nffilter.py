#!/usr/bin/python
# coding=utf-8


import argparse
import csv
import operator
import os
import sys
import json
import io
import operator
import time

from netaddr import IPNetwork, IPAddress


vern="1.4"
verdesc="walkdir"

servers = []

peers = {}


def dhms(sec, mode="dhms"):
    s=""
    intervals = (
        ('d',86400),
        ('h',3600),
        ('m',60),
        ('s',1)
    )
    
    started=False

    for suffix, num in intervals:
        if sec>=num or started:
            started = True
            c=sec/num            
            if mode == "dhms":
                s+="{}{} ".format(c,suffix)
            elif mode == ":":
                s+="{:02d}:".format(c)
            sec-=c*num
            
    # del trailing slash
    s = s.rstrip(': ')
    
    return s
    


def dir2file(path):
    if os.path.isdir(path):
        for file in os.listdir(path):
            if os.path.isfile(os.path.join(path, file)):
                yield os.path.join(path, file)
    else:
        yield path


#
# yield filenames
# unpacks lists, walks over dirs
#
def filelist(*args):
    for arg in args:
        if isinstance(arg,list):
            for f in filelist(*arg):
                for f in dir2file(f):
                    yield f                 
        else:
            for f in dir2file(arg):
                yield f         



"""
    SID
    comment
    goodpeers
    peers
    
"""

def goodclient(srv,caddr):
    ip = IPAddress(caddr)    
    
    for subnet in srv['goodpeers']:
        if subnet=='*':
            return True
        if ip in IPNetwork(subnet):
            return True
    return False

def addclient(srv,caddr):
    if not caddr in srv['peers']:
        # print "srv {} add peer {}".format(srv['SID'],caddr)
        srv['peers'].append(caddr)

def getserver(sid,servers):
    for s in servers:
        if s['SID']==sid:
            return s
    return None


def process(d, args):
    sid1 = d['proto']+':'+d['src']+':'+d['sport']
    sid2 = d['proto']+':'+d['dst']+':'+d['dport']


    # try both combination client/server
    for sc in [(sid1, d['dst']), (sid2,d['src'])]:
        sid = sc[0]
        client=sc[1]
        srv = getserver(sid,servers)
        if srv:
            # this is known server           
            if goodclient(srv,client):
                return 
            else:
                addclient(srv,client)
                return        

#    print "NOT KNOWN:",json.dumps(d, indent=4)

    # this not belongs to any known server
    for sc in [(sid1, d['dst'], sid2), (sid2,d['src'], sid1)]:
        sid = sc[0]
        client=sc[1]
        clientid = sc[2]

        # if we are here, this is not known server             
        if sid in peers:
            # print "found sid {} in peers".format(sid)
            
            if not clientid in peers[sid]:
                peers[sid].append(clientid)
                                    
            if len(peers[sid])>=args.n:
                # print ".. detected server {} ({} hits) ({} servers total)".format(sid, len(peers[sid]), len(servers))
                # add to servers
                s = dict()
                s['SID']=sid
                s['comment']='!!! NEW !!! Automatically added after {} hits. Set better comment.'.format(len(peers[sid]))
                s['goodpeers']=[]
                s['peers']=[]
                for client in peers[sid]:
                    clientip = client.split(':')[1]
                    if not clientip in s['peers']:
                        s['peers'].append(clientip)
                    
                
                servers.append(s)
            
        else:
            # print "create peer record for {} (other peer: {})".format(sid,sid2)
            # print "d:",json.dumps(d,indent=2)
            peers[sid]=[clientid]            
                        

parser = argparse.ArgumentParser(description='NetFlow filter. ver {} ({})'.format(vern, verdesc))
parser.add_argument('-f',nargs='+', dest='flist', help='CSV files')
parser.add_argument('-n', dest='n', help='number of flows to count peer as server', default=10, type=int)
parser.add_argument('-o', dest='outsrv', help='output servers file', default=None)
parser.add_argument('-i', dest='insrv', help='output servers file', default=None)

parser.add_argument('--peers', dest='peers', action='store_true', default=False, help='dump unhandled peers after run')
parser.add_argument('--sortpeers', dest='sortpeers', action='store_true', default=False, help='sort peers and goodpeers alphabetically')
parser.add_argument('--sortsids', dest='sortsids', action='store_true', default=False, help='sort by Server ID alphabetically')


args = parser.parse_args()

work = False

#if not args.flist:
#    print "no input files (-f)"
#    sys.exit(1)


if args.insrv:
    with open(args.insrv,'r') as infile:
        servers = json.load(infile)

    # validate list
    for s in servers:
        if not 'SID' in s:
            print "no SID!"
            print json.dumps(s,indent=4)
            sys.exit(1)
        if not 'comment' in s:
            s['comment']='!! no comment !!'
            
        if not 'peers' in s:
            s['peers']=[]
        
        if not 'goodpeers' in s:
            s['goodpeers']=[]         


time_start = int(time.time())

if args.flist:
    total = len(list(filelist(args.flist)))
    processed=0    
    for f in filelist(args.flist):
        processed+=1
        elapsed = int(time.time()) - time_start
        if elapsed==0:
            elapsed=1
        rate = float(processed)/elapsed
        avgtime = float(elapsed)/processed
        left = total - processed
        eta = int(left/rate)
        
        print "+ process ({}/{} {}s passed, ~{:.2f}s/each, ETA: {}) {}".format(processed,total,elapsed, avgtime, dhms(eta,':'), f)
        work=True
        with open(f,"r") as csvfile:
            csvr = csv.reader(csvfile)
            for row in csvr:
                d = {}
                d['proto']=row[7]
                d['src']=row[3]
                d['sport']=row[5]
                d['dst']=row[4]
                d['dport']=row[6]
                process(d,args)

# speers = sorted(peers.items(), key=operator.itemgetter(1))

# speers = speers[:-10]

#for p in speers:
#    print "server {} {} hits".format(p[0],p[1])

if args.sortpeers:
    work = True
    # sort peers/goodpeers alphabeticallys
    for s in servers:
        s['peers'] = sorted(s['peers'])
        s['goodpeers'] = sorted(s['goodpeers'])

if args.sortsids:
    work = True
    # sort peers/goodpeers alphabeticallys
    servers = sorted(servers, key=operator.itemgetter('SID'))


if not work:
    print "no work done"

if args.peers:
    print json.dumps(peers,indent=4)

if args.outsrv:
    with io.open(args.outsrv,'w',encoding='utf8') as outfile:
        data = json.dumps(servers, indent=4, sort_keys=True, ensure_ascii=False, encoding='utf8')
        outfile.write(unicode(data))
        
# print json.dumps(servers, indent=4)

                        
