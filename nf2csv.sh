#!/bin/sh

INDIR=netflows
OUTDIR=nfcsv

for f in `find $INDIR -type f -name "nfcapd.2*"`
do
    basef=`basename $f`
    outf=$OUTDIR/$basef.csv
    if [ -s $outf ]
    then
        echo .. skipping $f 
    else
        echo .. processing $f to $outf
        nfdump -q -o csv -r $f > $outf
    fi
done
