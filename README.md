﻿nffilter
===

Предназначение
---
nffilter предназначен для исследования сети через данные от NetFlow 
(сохраненнные в CSV формате: nfcapd -> nfdump -> nffilter). 

На основании "карты сети" (известные сервисы и их клиенты в JSON формате), он 
позволяет профильтровать данные netflow, и показать новые данные, которые не 
соответствуют этой карте (например, новые сервисы или новые клиенты).

Кроме того, nffilter может "догадаться" о сервисах (тот же протокол-адрес-порт, 
к которому соединяются несколько разных клиентов) и дополнять карту сети.

Подготовка входных файлов
---
Для работы требуются данные от NetFlow в CSV формате без заголовков, как их 
делает nfdump. Для удобства есть очень простой шелл-скрипт nf2csv.sh (17 строк),
который из netflow файлов в одном каталоге (INDIR) создает CSV файлы в другом 
каталоге (OUTDIR).

Ключи nffilter
---
```
xenon@braconnier:~/smsf/nffilter$ ./nffilter.py -h
usage: nffilter.py [-h] [-f FLIST [FLIST ...]] [-n N] [-o OUTSRV] [-i INSRV]
                   [--peers] [--sortpeers] [--sortsids]

NetFlow filter. ver 1.1 (sorting peers lists)

optional arguments:
  -h, --help            show this help message and exit
  -f FLIST [FLIST ...]  CSV files
  -n N                  number of flows to count peer as server
  -o OUTSRV             output servers file
  -i INSRV              output servers file
  --peers               dump unhandled peers after run
  --sortpeers           sort peers and goodpeers alphabetically
  --sortsids            sort by Server ID alphabetically
```


Генерация первоначальной карты сети
---
Для генерации карты сети, скрипт запуcкается без ключа -i (не подгружая карту 
сети, которой у нас еще нет), с ключом -o. Новым сервисом считается тот, для 
которого CSV файлах будет обнаружено более N (-n) различных потоков. Значение 
по-умолчанию 10, этого достаточно чтобы обнаружить наиболее активные сервисы без
 (или почти без) ложных срабатываний. Для обнаружения менее активных сервисов, 
 это значение стоит понизить.

Пример:
```
xenon@braconnier:~/smsf/nffilter$ ./nffilter.py -f nfcsv/* -o srv.json
+ process nfcsv/nfcapd.201607201835.csv
+ process nfcsv/nfcapd.201607201840.csv
+ process nfcsv/nfcapd.201607201845.csv
+ process nfcsv/nfcapd.201607201850.csv
+ process nfcsv/nfcapd.201607201855.csv
+ process nfcsv/nfcapd.201607201900.csv
+ process nfcsv/nfcapd.201607201905.csv
+ process nfcsv/nfcapd.201607201910.csv
+ process nfcsv/nfcapd.201607201915.csv
+ process nfcsv/nfcapd.201607201920.csv
+ process nfcsv/nfcapd.201607201925.csv
+ process nfcsv/nfcapd.201607201930.csv
+ process nfcsv/nfcapd.201607201935.csv
+ process nfcsv/nfcapd.201607201940.csv
+ process nfcsv/nfcapd.201607201945.csv
+ process nfcsv/nfcapd.201607201950.csv
```

В результате обнаруженные сервисы (карта сети) будут записаны в файл srv.json

Карта сети 
---
Карта сети - хранится в JSON формате, представляет из себя список сервисов, 
каждый сервис описывается структурой с полями:
SID - идентификатор сервера, строка формата PROTO:IPv4:PORT, например "TCP:10.77.77.65:80".
Если протокол не подразумевает порт (например, ICMP), значение порта - 0.
comment - текстовый комментарий к сервису. Изначально заполняется шаблонным 
значением, затем его нужно заменить на актуальное описание сервиса. 
goodpeers - список разрешенных хостов или подсетей для работы с этим сервисом. 
Каждый элемент - либо IPv4 адрес хоста ("1.2.3.4"), либо подсеть ("10.77.77.0/24"), либо "*" (соответствует любому адресу).
peers - список фактических клиентов сервиса, которые не входят в goodpeers. nffilter дополняет этот список по ходу работы.

Пример записи о сервисе:
```
  {
        "SID": "TCP:10.77.77.34:80",
        "comment": "!! NEW !! Automatically added after 10 hits",
        "goodpeers": [],
        "peers": [
            "10.77.77.203",
            "10.77.77.238",
            "10.77.77.218"
        ]
    },
```

Обнаружение новых клиентов
---
После составления карты сети, нужно заполнить список goodpeers для каждого сервиса. При последующих запусках (с ключами -i srv.json -i srv.json) nffilter будет проверять всех клиентов из netflow и запишет в peers тех, кто не соответствует goodpeers.

Обнаружение новых сервисов
---
Активный сервис будет обнаружен автоматически, и (при ключе -o) будет записан в файл карты сети.

Может быть так, что малоактивный сервис не будет иметь достаточно трафика, чтобы быть обнаруженным в качестве сервиса (-n). Кроме того, при n=0 (сервис будет обнаружен даже при одном (1>0) потоке данных) - нет возможности надежно определить, какая сторона соединения является клиентом, а какая сервером.

При ключе --peers nffilter в конце работы распечатает свою структуру обнаруженных пиров (чьи соединения не относятся ни к одному из обнаруженных сервисов):

```   
xenon@braconnier:~/smsf/nffilter$ ./nffilter.py -f nfcsv/* -o srv.json -i srv.json --peers
+ process nfcsv/nfcapd.201607201835.csv
...
+ process nfcsv/nfcapd.201607201950.csv
{
    "TCP:10.130.10.130:62643": [
        "TCP:10.54.102.39:51010"
    ], 
    "TCP:10.54.102.62:62479": [
        "TCP:10.77.77.28:22"
    ], 
...
    "TCP:10.77.77.65:49086": [
        "TCP:173.194.32.190:80"
    ]
}
```

Сортировка
---
Для удобства nffilter может сортировать записи в файле карты. Используйте ключи `--sortpeers` и `--sortsids` вместе с ключем `-o`.










